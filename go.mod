module gitlab.com/sean__ch0n/proglog

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tysontate/gommap v0.0.0-20210506040252-ef38c88b18e1 // indirect
)
