package main

import (
	"log"

	"gitlab.com/sean__ch0n/proglog/internal/server"
)

func main() {
	srv := server.NewHTTPServer(":8080")
	log.Fatal(srv.ListenAndServe())
}
